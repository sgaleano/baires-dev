import React, { Component } from 'react';

// Components
import Album from '../../components/Album/Album';

class AlbumContainer extends Component {

    constructor() {
        super();

        this.state = {
            albums: []
        };
    }

    componentDidMount() {
        fetch('https://jsonplaceholder.typicode.com/photos')
            .then(response => response.json())
            .then(json => {
                let albums = [];
                json.forEach(e => {
                    if (albums[e.albumId]) {
                        albums[e.albumId].push(e);
                    } else {
                        albums[e.albumId] = [e];
                    }
                });
                const first = albums.length - 3;
                let newArray = albums.slice(first);
                this.setState({ albums: newArray });
            });
    }

    render() {
        const { albums } = this.state;
        return (
            <div className="AlbumContainer">
                {
                    albums.map( e => {
                        return (<Album key={e.id} info={e} />)
                    })
                }
            </div>
        );
    }

}

export default AlbumContainer;