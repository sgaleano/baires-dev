import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

// Components
import AlbumContainer from './modules/Album/AlbumContainer';

class App extends Component {
  render() {
    return (
      <AlbumContainer />
    );
  }
}

export default App;
