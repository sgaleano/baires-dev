import React from 'react';

import './Album.css';

const Album = (props) => {

    const albumLength = props.info.length - 1;

    return (
        <div className="Album">
            <div>album title</div>
            <img src={props.info[albumLength].thumbnailUrl} alt=""/>
            <img src={props.info[albumLength - 1].thumbnailUrl} alt=""/>
        </div>
    )

}

export default Album;